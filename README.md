# Docker images with Tex Live

Create docker images for data analysis.
This project allows to build images also with different TexLive versions.
Images are installed using [https://gitlab.cern.ch/ci-tools/docker-image-builder](https://gitlab.cern.ch/ci-tools/docker-image-builder).
A similar project that uses this is [https://gitlab.cern.ch/ci-tools/ci-worker](https://gitlab.cern.ch/ci-tools/ci-worker).

In order to contribute to the package, you must fork it and create a merge request.
In order to build a new image, modify [.gitlab-ci.yml](.gitlab-ci.yml)
including a new job, whose content must be similar to
```
cc7:
  <<: *docker-build
  variables:
    CONTEXT_DIR: ci-base
    FROM: gitlab-registry.cern.ch/ci-tools/ci-worker:cc7
    TO: $CI_REGISTRY_IMAGE/cc7:latest

cc7-texlive-2020:
  <<: *docker-build
  variables:
    BUILD_ARG: YEAR=2020
    CONTEXT_DIR: ci-texlive
    FROM: gitlab-registry.cern.ch/ci-tools/ci-worker:cc7
    TO: $CI_REGISTRY_IMAGE/cc7-texlive:2020
```
Jobs must extend the definition of `docker-build`.
The context directory determines whether TeX Live will be installed or not.
If running with it being installed, you must include the `YEAR` argument
(which determines the `TexLive` version to use)
In both cases, the base image is specified in `FROM` and the target in `TO`.
A specific label with the name of the job must be created in order to properly
build the image when attempting to merge.
The label `all` is reserved to build all the available images, while
`documentation` is reserved for modifications of the [README.md](README.md).

By following this procedure, the docker images are not installed in the registry
of this repository but on that of the fork.